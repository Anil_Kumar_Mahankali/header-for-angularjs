import angular from 'angular';
import directive from './directive';

export default angular
    .module('header.module', [])
    .directive(directive.name, directive);