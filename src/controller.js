export default class HeaderController {

    constructor(sessionManager,
                partnerRepServiceSdk,
                $q) {

        this._userInfo = null;
        this._sessionManager = sessionManager;
        this._partnerRepServiceSdk = partnerRepServiceSdk;



        if (this.isLoginRequired()) {

            /*$q(resolve =>
                    resolve(this._sessionManager.getUserInfo())
            ).then((userInfo) => {
                this._userInfo = userInfo;
            });*/

            Promise.all(
                [
                    this._sessionManager.getUserInfo(),
                    this._sessionManager.getAccessToken()
                ]
            ).then(results => {
                $q(resolve => {
                    resolve(this._partnerRepServiceSdk
                        .getPartnerRepWithId(results[0]._sub,results[1]));
                })
                    .then(response => {
                        this._userInfo = results[0];
                        this.sapAccountNumber = response.sapAccountNumber;
                    });
            });

        }
    }

    get userInfo() {
        return this._userInfo;
    }

    logout() {
        this._sessionManager.logout();
    }

}

HeaderController.$inject = [
    'sessionManager',
    'partnerRepServiceSdk',
    '$q'
];
